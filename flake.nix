# flake.nix
{
  description = "My own Neovim flake";
  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-unstable";
    };
    github_theme_src = {
      url = "github:projekt0n/github-nvim-theme";
      flake = false;
    };
    neorg_telescope_src = {
      url = "github:nvim-neorg/neorg-telescope";
      flake = false;
    };
    conform_src = {
      url = "github:stevearc/conform.nvim";
      flake = false;
    };
    wrapping_src = {
      url = "github:andrewferrier/wrapping.nvim";
      flake = false;
    };
    window_picker_src = {
      url = "github:s1n7ax/nvim-window-picker";
      flake = false;
    };
    llm_src = {
      url = "github:huggingface/llm.nvim";
      flake = false;
    };
    lua-utils_src = {
      url = "github:nvim-neorg/lua-utils.nvim";
      flake = false;
    };
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
  };

  outputs = { self, nixpkgs, github_theme_src, conform_src, wrapping_src, window_picker_src, neorg_telescope_src, llm_src, neovim-nightly-overlay, lua-utils_src }:
    let
      overlayFlakeInputs = prev: final: {
        #neovim = neovim.packages.x86_64-linux.neovim;

        vimPlugins = final.vimPlugins // {
          github_theme = import ./packages/vimPlugins/github_theme.nix {
            src = github_theme_src;
            pkgs = prev;
          };
          neorg-telescope = import ./packages/vimPlugins/neorg-telescope.nix {
            src = neorg_telescope_src;
            pkgs = prev;
          };
          llm = import ./packages/vimPlugins/llm.nix {
            src = llm_src;
            pkgs = prev;
          };
          lua-utils = import ./packages/vimPlugins/lua-utils.nix {
            src = lua-utils_src;
            pkgs = prev;
          };
          conform = import ./packages/vimPlugins/conform.nix {
            src = conform_src;
            pkgs = prev;
          };
          wrapping = import ./packages/vimPlugins/wrapping.nix {
            src = wrapping_src;
            pkgs = prev;
          };
          window_picker = import ./packages/vimPlugins/window_picker.nix {
            src = window_picker_src;
            pkgs = prev;
          };
        };
      };

      overlayMyNeovim = prev: final: {
        myNeovim = import ./packages/myNeovim.nix {
          pkgs = final;
        };
      };

      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [ overlayFlakeInputs neovim-nightly-overlay.overlays.default overlayMyNeovim ];
      };

    in
    {
      packages.x86_64-linux.default = pkgs.myNeovim;
      apps.x86_64-linux.default = {
        type = "app";
        program = "${pkgs.myNeovim}/bin/nvim";
      };
    };
}
