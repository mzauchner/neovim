vim.keymap.set('n', '<C-l>', '<cmd>noh<CR>', { silent = true }) -- Clear highlights
vim.keymap.set('n', '<Leader>f', ':<C-u>ClangFormat<CR>', { silent = true, desc = "Clang format" })
vim.keymap.set('n', '<C-J>', '<C-W><C-J>', { silent = true, desc = "jump window down" })
vim.keymap.set('n', '<C-K>', '<C-W><C-K>', { silent = true, desc = "jump window up" })

vim.keymap.set('n', '<C-L>', '<C-W><C-L>', { silent = true, desc = "jump window right" })

vim.keymap.set('n', '<C-H>', '<C-W><C-H>', { silent = true, desc = "jump window left" })
vim.keymap.set('n', '<Leader>n', ':Neotree toggle<CR>', { silent = true, desc = "Neotree toggle" })
vim.keymap.set('n', '<Tab>', ':bnext<CR>', { silent = true, desc = "next buffer" })

--yank in  normal mode
vim.keymap.set('n', '<C-c>', '"+y', { silent = true, desc = "yank in normal mode" })
--paste  in normal mode
vim.keymap.set('n', '<C-x>', '"+p', { silent = true, desc = "paste in normal mode" })
--vim.keymap.set('i', '<C-v>', '<ESC>"+pa', { silent = true, desc = "parens" })
-- dap stuff
vim.keymap.set('n', '<Leader>db', require('dap').toggle_breakpoint, { silent = true, desc = "Toggle breakpoint" })
vim.keymap.set('n', '<Leader>dc', require('dap').continue, { silent = true, desc = "Start debugger" })
vim.keymap.set('n', '<Leader>dn', require('dap').step_over, { silent = true, desc = "DAP step over" })
vim.keymap.set('n', '<Leader>ds', require('dap').step_into, { silent = true, desc = "DAP step into" })
local wrap = function(func, ...)
    local args = { ... }
    return function()
        func(unpack(args))
    end
end
--vim.keymap.set('n', '<Leader>dt', wrap(require('neotest').run.run, { strategy = 'dap' }),
--{ silent = true, desc = "Test nearest method" })

--vim.keymap.set('n', '<Leader>dT', wrap(require('neotest').run.run, { vim.fn.expand('%'), strategy = 'dap' }),
--{ silent = true, desc = "Run all tests in file" })


--local crates = require('crates')
--local opts = { noremap = true, silent = true }
--opts.desc = "crates toggle"
--
--vim.keymap.set('n', '<leader>ct', crates.toggle, opts)
--opts.desc = "crates reload"
--vim.keymap.set('n', '<leader>cr', crates.reload, opts)
--
--opts.desc = "crates show versions popup"
--vim.keymap.set('n', '<leader>cv', crates.show_versions_popup, opts)
--opts.desc = "crates show features popup"
--vim.keymap.set('n', '<leader>cf', crates.show_features_popup, opts)
--opts.desc = "crates show dependencies popup"
--vim.keymap.set('n', '<leader>cd', crates.show_dependencies_popup, opts)
--
--opts.desc = "update crate"
--vim.keymap.set('n', '<leader>cu', crates.update_crate, opts)
--opts.desc = "update crates"
--vim.keymap.set('v', '<leader>cu', crates.update_crates, opts)
--opts.desc = "update all crates"
--vim.keymap.set('n', '<leader>ca', crates.update_all_crates, opts)
--opts.desc = "upgrade crate"
--vim.keymap.set('n', '<leader>cU', crates.upgrade_crate, opts)
--opts.desc = "upgrade crates"
--vim.keymap.set('v', '<leader>cU', crates.upgrade_crates, opts)
--opts.desc = "upgrade crates"
--vim.keymap.set('n', '<leader>cA', crates.upgrade_all_crates, opts)
--
--opts.desc = "open crate homepage"
--vim.keymap.set('n', '<leader>cH', crates.open_homepage, opts)
--opts.desc = "open crate repo"
--vim.keymap.set('n', '<leader>cR', crates.open_repository, opts)
--opts.desc = "open crate documentation"
--vim.keymap.set('n', '<leader>cD', crates.open_documentation, opts)
--opts.desc = "open crates.io"
--vim.keymap.set('n', '<leader>cC', crates.open_crates_io, opts)

-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer

-- Displays hover information about the symbol under the cursor
vim.keymap.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>')

-- Jump to the definition
vim.keymap.set('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>')

-- Jump to declaration
vim.keymap.set('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>')

-- Lists all the implementations for the symbol under the cursor
vim.keymap.set('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>')

-- Jumps to the definition of the type symbol
vim.keymap.set('n', 'go', '<cmd>lua vim.lsp.buf.type_definition()<cr>')

-- Lists all the references
vim.keymap.set('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>')

-- Displays a function's signature information
vim.keymap.set('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>')

-- Renames all references to the symbol under the cursor
vim.keymap.set('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>')

-- Selects a code action available at the current cursor position
vim.keymap.set('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>')
vim.keymap.set('x', '<F4>', '<cmd>lua vim.lsp.buf.range_code_action()<cr>')

-- Show diagnostics in a floating window
vim.keymap.set('n', 'gl', '<cmd>lua vim.diagnostic.open_float()<cr>')

-- Move to the previous diagnostic
vim.keymap.set('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<cr>')

-- Move to the next diagnostic
vim.keymap.set('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<cr>')
vim.keymap.set('n', '<C-m>', '<C-W>-', { silent = true, desc = "decrease height" })
vim.keymap.set('n', '<C-p>', '<C-W>+', { silent = true, desc = "increase height" })
vim.keymap.set('n', '<C-<>', '<C-W><', { silent = true, desc = "decrease width" })
vim.keymap.set('n', '<C->>', '<C-W>>', { silent = true, desc = "increase width" })
