# packages/vimPlugins/github_theme.nix
{ pkgs, src }:
pkgs.vimUtils.buildVimPlugin {
  name = "github_theme";
  inherit src;
}
