# packages/vimPlugins/github_theme.nix
{ pkgs, src }:
pkgs.vimUtils.buildVimPlugin {
  name = "neorg-telescope";
  inherit src;
}
