-- IMPORTANT: make sure to setup neodev BEFORE lspconfig
local utils = {}

local scopes = { o = vim.o, b = vim.bo, w = vim.wo }

function utils.opt(scope, key, value)
    scopes[scope][key] = value
    if scope ~= 'o' then scopes['o'][key] = value end
end

require("which-key").setup {}


utils.opt('w', 'number', true)
utils.opt('w', 'relativenumber', true)
local getword = function()
    local builtin = require('telescope.builtin')
    local opts = { query = vim.fn.expand("<cword>") }
    builtin.lsp_workspace_symbols(opts)
    --return "success"
end

local cmd = vim.cmd
local indent = 2
-- Highlight on yank
--
--
--
cmd 'au TextYankPost * lua vim.highlight.on_yank {on_visual = false}'
cmd 'syntax enable'
cmd 'filetype indent on'
utils.opt('b', 'shiftwidth', 4)
cmd 'set cindent shiftwidth=4'
--cmd 'colorscheme material'
cmd 'set path+=**'
--cmd 'inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>'
--cmd 'inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"'
vim.o.backup = false
vim.o.wb = false
vim.o.autoread = true
vim.o.showcmd = true
vim.o.lazyredraw = true
vim.o.encoding = 'utf-8'
vim.o.ttyfast = true
vim.o.ttimeoutlen = 10
--vim.o.t_Co="256"
vim.o.lazyredraw = true
vim.o.scrolloff = 8
vim.o.swapfile = false
vim.o.wildmenu = true
vim.o.wrap = false





utils.opt('b', 'expandtab', true)
utils.opt('b', 'tabstop', indent)
utils.opt('o', 'hidden', true)
utils.opt('o', 'ignorecase', true)
utils.opt('o', 'shiftround', true)
utils.opt('o', 'smartcase', true)
utils.opt('o', 'smartindent', true)
utils.opt('o', 'splitbelow', true)
utils.opt('o', 'splitright', true)
utils.opt('o', 'termguicolors', true)
utils.opt('o', 'clipboard', 'unnamed,unnamedplus')


--utils.ops('o', 'nobackup', true)
--utils.ops('o', 'nowb', true)
--utils.ops('o', 'autoread', true)
--utils.ops('o', 'scrolloff', 8)
--utils.ops('o', 'encoding', 'utf-8')
--utils.ops('o', 'background', 'dark')
--utils.ops('o', 't_Co', 256)
--utils.ops('o', 'history', 1000)
--vim.opt('o', 'scrolloff', 4 )
--
--utils.opt('o', 'hlsearch', true)
utils.opt('o', 'incsearch', true)
utils.opt('o', 'smartcase', true)


-- Setup nvim-cmp.
require('luasnip.loaders.from_vscode').lazy_load()
local cmp = require 'cmp'
local has_words_before = function()
    if vim.api.nvim_buf_get_option(0, "buftype") == "prompt" then return false end
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_text(0, line - 1, 0, line - 1, col, {})[1]:match("^%s*$") == nil
end

local luasnip = require('luasnip')
cmp.setup({
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end
    },
    mapping = {
        ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
        ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
        ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
        ['<C-Space>'] = cmp.mapping.complete({}),
        ["<Tab>"] = vim.schedule_wrap(function(fallback)
            if cmp.visible() and has_words_before() then
                cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
            else
                fallback()
            end
        end),
        ["<S-Tab>"] = cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Select },
        ['<C-e>'] = cmp.mapping({
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
        }),
        ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    },
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        --{ name = 'vsnip' }, -- For vsnip users.
        --{ name = 'luasnip' }, -- For luasnip users.
        -- { name = 'ultisnips' }, -- For ultisnips users.
        -- { name = 'snippy' }, -- For snippy users.
        { name = "luasnip" },
        --{ name = "copilot" },
        { name = 'buffer' },
    }
    ),
    formatting = {
        format = require('lspkind').cmp_format({
            mode = "text_symbol",
            max_width = 50,
            symbol_map = {
                Copilot = "",
                Text = "",
                Method = "",
                Function = "",
                Constructor = "",
                Field = "ﰠ",
                Variable = "",
                Class = "ﴯ",
                Interface = "",
                Module = "",
                Property = "ﰠ",
                Unit = "塞",
                Value = "",
                Enum = "",
                Keyword = "",
                Snippet = "",
                Color = "",
                File = "",
                Reference = "",
                Folder = "",
                EnumMember = "",
                Constant = "",
                Struct = "פּ",
                Event = "",
                Operator = "",
                TypeParameter = ""


            }
        })
    },
    window = {
        completion = {
            -- rounded border; thin-style scrollbar
            border = 'rounded',
            scrollbar = '║',
        },
        documentation = {
            -- no border; native-style scrollbar
            border = 'rounded',
            scrollbar = '║',
            -- other options
        },
    },
})

-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline('/', {
    sources = {
        { name = 'buffer' }
    }
})

--Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
    sources = cmp.config.sources({
        { name = 'path' }
    }, {
        { name = 'cmdline' }
    })
})

-- Setup lspconfig.



--[




local vim = vim
local opt = vim.opt

opt.foldmethod = "expr"
opt.foldexpr = "nvim_treesitter#foldexpr()"
local vim = vim
local api = vim.api
local telescope = require('telescope')

telescope.setup {
    -- move this to the place where you call the telescope setup function
            extensions = {
                advanced_git_search = 
                    {
    -- Browse command to open commits in browser. Default fugitive GBrowse.
    -- {commit_hash} is the placeholder for the commit hash.
    browse_command = "GBrowse {commit_hash}",
    -- when {commit_hash} is not provided, the commit will be appended to the specified command seperated by a space
    -- browse_command = "GBrowse",
    -- => both will result in calling `:GBrowse commit`

    -- fugitive or diffview
    diff_plugin = "fugitive",
    -- customize git in previewer
    -- e.g. flags such as { "--no-pager" }, or { "-c", "delta.side-by-side=false" }
    git_flags = {},
    -- customize git diff in previewer
    -- e.g. flags such as { "--raw" }
    git_diff_flags = {},
    -- Show builtin git pickers when executing "show_custom_functions" or :AdvancedGitSearch
    show_builtin_git_pickers = false,
    entry_default_author_or_date = "author", -- one of "author" or "date"
    keymaps = {
        -- following keymaps can be overridden
        toggle_date_author = "<C-w>",
        open_commit_in_browser = "<C-o>",
        copy_commit_hash = "<C-y>",
        show_entire_commit = "<C-e>",
    },

    -- Telescope layout setup
    telescope_theme = {
        function_name_1 = {
            -- Theme options
        },
        function_name_2 = "dropdown",
        -- e.g. realistic example
        show_custom_functions = {
            layout_config = { width = 0.4, height = 0.4 },
        },

    },
}
            },

    pickers = {
        find_files = {

            hidden = true

        }

    }
}
require("telescope").load_extension("advanced_git_search")
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, { desc = "Telescope find files" })
vim.keymap.set('n', '<leader>fg', builtin.live_grep, { desc = "Telescope live grep" })
vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = "Telescope find buffers" })
vim.keymap.set('n', '<leader>fh', builtin.help_tags, { desc = "Telescope find help tags" })
vim.keymap.set('n', '<leader>fs', builtin.lsp_document_symbols, { desc = "Telescope find doc symbols" })
vim.keymap.set('n', '<leader>fw', getword, { silent = true, desc = "Telescope find word under cusor" })
--
vim.cmd([[ let g:neo_tree_remove_legacy_commands = 1 ]])
local opts = {
    highlight_hovered_item = true,
    show_guides = true,
    auto_preview = false,
    position = 'right',
    relative_width = true,
    width = 25,
    auto_close = false,
    show_numbers = false,
    show_relative_numbers = false,
    show_symbol_details = true,
    preview_bg_highlight = 'Pmenu',
    autofold_depth = nil,
    auto_unfold_hover = true,
    fold_markers = { '', '' },
    wrap = false,
    keymaps = {
        -- These keymaps can be a string or a table for multiple keys
        close = { "<Esc>", "q" },
        goto_location = "<Cr>",
        focus_location = "o",
        hover_symbol = "<C-space>",
        toggle_preview = "K",
        rename_symbol = "r",
        code_actions = "a",
        fold = "h",
        unfold = "l",
        fold_all = "W",
        unfold_all = "E",
        fold_reset = "R",
    },
    lsp_blacklist = {},
    symbol_blacklist = {},
    symbols = {
        File = { icon = "", hl = "TSURI" },
        Module = { icon = "", hl = "TSNamespace" },
        Namespace = { icon = "", hl = "TSNamespace" },
        Package = { icon = "", hl = "TSNamespace" },
        Class = { icon = "𝓒", hl = "TSType" },
        Method = { icon = "ƒ", hl = "TSMethod" },
        Property = { icon = "", hl = "TSMethod" },
        Field = { icon = "", hl = "TSField" },
        Constructor = { icon = "", hl = "TSConstructor" },
        Enum = { icon = "ℰ", hl = "TSType" },
        Interface = { icon = "ﰮ", hl = "TSType" },
        Function = { icon = "", hl = "TSFunction" },
        Variable = { icon = "", hl = "TSConstant" },
        Constant = { icon = "", hl = "TSConstant" },
        String = { icon = "𝓐", hl = "TSString" },
        Number = { icon = "#", hl = "TSNumber" },
        Boolean = { icon = "⊨", hl = "TSBoolean" },
        Array = { icon = "", hl = "TSConstant" },
        Object = { icon = "⦿", hl = "TSType" },
        Key = { icon = "🔐", hl = "TSType" },
        Null = { icon = "NULL", hl = "TSType" },
        EnumMember = { icon = "", hl = "TSField" },
        Struct = { icon = "𝓢", hl = "TSType" },
        Event = { icon = "🗲", hl = "TSType" },
        Operator = { icon = "+", hl = "TSOperator" },
        TypeParameter = { icon = "𝙏", hl = "TSParameter" }
    }
}
require("symbols-outline").setup(opts)
require("onedarkpro").setup({
    styles = {
        types = "NONE",
        methods = "italic",
        numbers = "NONE",
        strings = "NONE",
        comments = "italic",
        keywords = "bold,italic",
        constants = "NONE",
        functions = "italic",
        operators = "NONE",
        variables = "NONE",
        parameters = "NONE",
        conditionals = "italic",
        virtual_text = "NONE",
    }
})
vim.cmd("colorscheme onedark")

--require('material').setup({
--
--    contrast = {
--        terminal = false,            -- Enable contrast for the built-in terminal
--        sidebars = false,            -- Enable contrast for sidebar-like windows ( for example Nvim-Tree )
--        floating_windows = false,    -- Enable contrast for floating windows
--        cursor_line = true,          -- Enable darker background for the cursor line
--        non_current_windows = false, -- Enable contrasted background for non-current windows
--        filetypes = {},              -- Specify which filetypes get the contrasted (darker) background
--    },
--
--    styles = { -- Give comments style such as bold, italic, underline etc.
--        comments = { italic = true },
--        strings = { --[[ bold = true ]] },
--        keywords = { italic = true },
--        functions = { italic = true },
--        variables = {},
--        operators = {},
--        types = {},
--    },
--
--    plugins = { -- Uncomment the plugins that you use to highlight them
--        -- Available plugins:
--        "dap",
--        -- "dashboard",
--        -- "eyeliner",
--        -- "fidget"
--        -- "flash"
--        -- "gitsigns",
--        -- "harpoon",
--        -- "hop",
--        -- "illuminate",
--        -- "indent-blankline",
--        -- "lspsaga",
--        -- "mini",
--        -- "neogit",
--        --"neotest",
--        -- "neorg",
--        -- "noice"
--        -- "nvim-cmp",
--        -- "nvim-navic",
--        -- "nvim-tree",
--        -- "nvim-web-devicons",
--        -- "rainbow-delimiters",
--        -- "sneak",
--        "telescope",
--        -- "trouble",
--        "which-key",
--    },
--
--    disable = {
--        colored_cursor = true, -- Disable the colored cursor
--        borders = false,       -- Disable borders between verticaly split windows
--        background = false,    -- Prevent the theme from setting the background (NeoVim then uses your terminal background)
--        term_colors = false,   -- Prevent the theme from setting terminal colors
--        eob_lines = false      -- Hide the end-of-buffer lines
--    },
--
--    high_visibility = {
--        lighter = false, -- Enable higher contrast text for lighter style
--        darker = true    -- Enable higher contrast text for darker style
--    },
--
--    lualine_style = "default", -- Lualine style ( can be 'stealth' or 'default' )
--
--    async_loading = true,      -- Load parts of the theme asyncronously for faster startup (turned on by default)
--
--    custom_colors = nil,       -- If you want to override the default colors, set this to a function
--
--    custom_highlights = {},    -- Overwrite highlights with your own
--})
--vim.g.material_style = "darker"
--vim.cmd 'colorscheme material'

--require('material').setup({
--contrast = {
--terminal = true,             -- Enable contrast for the built-in terminal
--sidebars = false,            -- Enable contrast for sidebar-like windows ( for example Nvim-Tree )
--floating_windows = false,    -- Enable contrast for floating windows
--cursor_line = false,         -- Enable darker background for the cursor line
--non_current_windows = false, -- Enable darker background for non-current windows
--filetypes = {},              -- Specify which filetypes get the contrasted (darker) background
--},
--styles = {                       -- Give comments style such as bold, italic, underline etc.
--comments = { --[[ italic = true ]] },
--strings = { --[[ bold = true ]] },
--keywords = { --[[ underline = true ]] },
--functions = { --[[ bold = true, undercurl = true ]] },
--variables = {},
--operators = {},
--types = {},
--},
--plugins = { -- Uncomment the plugins that you use to highlight them
-- Available plugins:
-- "dap",
-- "dashboard",
-- "gitsigns",
-- "hop",
-- "indent-blankline",
-- "lspsaga",
-- "mini",
--"neogit",
-- "nvim-cmp",
-- "nvim-navic",
-- "nvim-tree",
--"nvim-web-devicons",
-- "sneak",
--"telescope",
-- "trouble",
--"which-key",
--},
--disable = {
--colored_cursor = false, -- Disable the colored cursor
--borders = false,        -- Disable borders between verticaly split windows
--background = true,      -- Prevent the theme from setting the background (NeoVim then uses your terminal background)
--term_colors = true,     -- Prevent the theme from setting terminal colors
--eob_lines = false       -- Hide the end-of-buffer lines
--},
--high_visibility = {
--lighter = true,        -- Enable higher contrast text for lighter style
--darker = true          -- Enable higher contrast text for darker style
--},
--lualine_style = "default", -- Lualine style ( can be 'stealth' or 'default' )
--async_loading = true,      -- Load parts of the theme asyncronously for faster startup (turned on by default)
--custom_highlights = {},    -- Overwrite highlights with your own
--custom_colors = function(colors)
--colors.syntax.comments = "#006600"
--colors.editor.disabled = "#ff9900"
--end
----})
--vim.g.material_style = "darker"
--cmd 'hi!Normal ctermbg=NONE guibg=None'
require('lualine').setup {
    options = {
        icons_enabled = true,
        theme = 'auto',
        transparent = true,
        component_separators = { left = '', right = '' },
        section_separators = { left = '', right = '' },
        disabled_filetypes = {
            statusline = {},
            winbar = {},
        },
        statusline = {},
        always_divide_middle = true,
        globalstatus = false,
        refresh = {
            statusline = 1000,
            tabline = 1000,
            winbar = 1000,
        }
    },
    sections = {
        lualine_a = { 'mode' },
        lualine_b = { 'branch', 'diff', 'diagnostics' },
        lualine_c = { 'filename' },
        lualine_x = { 'encoding', 'fileformat', 'filetype' },
        lualine_y = { 'progress' },
        lualine_z = { 'location' }
    },
    inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = { 'filename' },
        lualine_x = { 'location' },
        lualine_y = {},
        lualine_z = {}
    },
    tabline = {},
    winbar = {},
    inactive_winbar = {},
    extensions = {},

}


local rt = require("rust-tools")
local function on_attach(client, buffer)
    -- This callback is called when the LSP is atttached/enabled for this buffer
    -- we could set keymaps related to LSP, etc here.
end

local opts = {
    tools = {
        -- rust-tools options

        -- how to execute terminal commands
        -- options right now: termopen / quickfix
        executor = require("rust-tools.executors").termopen,

        -- callback to execute once rust-analyzer is done initializing the workspace
        -- The callback receives one parameter indicating the `health` of the server: "ok" | "warning" | "error"
        on_initialized = nil,

        -- automatically call RustReloadWorkspace when writing to a Cargo.toml file.
        reload_workspace_from_cargo_toml = true,

        -- These apply to the default RustSetInlayHints command
        inlay_hints = {
            -- automatically set inlay hints (type hints)
            -- default: true
            auto = true,

            -- Only show inlay hints for the current line
            only_current_line = false,

            -- whether to show parameter hints with the inlay hints or not
            -- default: true
            show_parameter_hints = true,

            -- prefix for parameter hints
            -- default: "<-"
            parameter_hints_prefix = "<- ",

            -- prefix for all the other hints (type, chaining)
            -- default: "=>"
            other_hints_prefix = "=> ",

            -- whether to align to the length of the longest line in the file
            max_len_align = false,

            -- padding from the left if max_len_align is true
            max_len_align_padding = 1,

            -- whether to align to the extreme right or not
            right_align = false,

            -- padding from the right if right_align is true
            right_align_padding = 7,

            -- The color of the hints
            highlight = "Comment",
        },

        -- options same as lsp hover / vim.lsp.util.open_floating_preview()
        hover_actions = {
            -- the border that is used for the hover window
            -- see vim.api.nvim_open_win()
            border = {
                { "╭", "FloatBorder" },
                { "─", "FloatBorder" },
                { "╮", "FloatBorder" },
                { "│", "FloatBorder" },
                { "╯", "FloatBorder" },
                { "─", "FloatBorder" },
                { "╰", "FloatBorder" },
                { "│", "FloatBorder" },
            },

            -- Maximal width of the hover window. Nil means no max.
            max_width = nil,

            -- Maximal height of the hover window. Nil means no max.
            max_height = nil,

            -- whether the hover action window gets automatically focused
            -- default: false
            auto_focus = false,
        },

        -- settings for showing the crate graph based on graphviz and the dot
        -- command
        crate_graph = {
            -- Backend used for displaying the graph
            -- see: https://graphviz.org/docs/outputs/
            -- default: x11
            backend = "x11",
            -- where to store the output, nil for no output stored (relative
            -- path from pwd)
            -- default: nil
            output = nil,
            -- true for all crates.io and external crates, false only the local
            -- crates
            -- default: true
            full = true,

            -- List of backends found on: https://graphviz.org/docs/outputs/
            -- Is used for input validation and autocompletion
            -- Last updated: 2021-08-26
            enabled_graphviz_backends = {
                "bmp",
                "cgimage",
                "canon",
                "dot",
                "gv",
                "xdot",
                "xdot1.2",
                "xdot1.4",
                "eps",
                "exr",
                "fig",
                "gd",
                "gd2",
                "gif",
                "gtk",
                "ico",
                "cmap",
                "ismap",
                "imap",
                "cmapx",
                "imap_np",
                "cmapx_np",
                "jpg",
                "jpeg",
                "jpe",
                "jp2",
                "json",
                "json0",
                "dot_json",
                "xdot_json",
                "pdf",
                "pic",
                "pct",
                "pict",
                "plain",
                "plain-ext",
                "png",
                "pov",
                "ps",
                "ps2",
                "psd",
                "sgi",
                "svg",
                "svgz",
                "tga",
                "tiff",
                "tif",
                "tk",
                "vml",
                "vmlz",
                "wbmp",
                "webp",
                "xlib",
                "x11",
            },
        },
    },
    -- all the opts to send to nvim-lspconfig
    -- these override the defaults set by rust-tools.nvim
    -- see https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#rust_analyzer
    server = {
        -- on_attach is a callback called when the language server attachs to the buffer
        --
        on_attach = on_attach,
        standalone = true,
        settings = {
            -- to enable rust-analyzer settings visit:
            -- https://github.com/rust-analyzer/rust-analyzer/blob/master/docs/user/generated_config.adoc
            ["rust-analyzer"] = {
                -- enable clippy on save
                cargo = {
                    features = { "ssr" },
                    loadOutDirsFromCheck = true,
                    buildScripts = {
                        enable = true,
                    }
                },
                check = {
                    features = { "ssr" },
                },
                -- Add clippy lints for Rust.
                checkOnSave = {
                    features = { "ssr" },
                    command = "clippy",
                    extraArgs = { "--no-deps" },
                },
            },
        },
    },
    -- debugging stuff
    dap = {
        adapter = {
            type = "executable",
            command = "lldb-vscode",
            name = "rt_lldb",
        },
    },
}
require('rust-tools').setup(opts)

-- LSP Diagnostics Options Setup
local sign = function(opts)
    vim.fn.sign_define(opts.name, {
        texthl = opts.name,
        text = opts.text,
        numhl = ''
    })
end

sign({ name = 'DiagnosticSignError', text = '' })
sign({ name = 'DiagnosticSignWarn', text = '' })
sign({ name = 'DiagnosticSignHint', text = '' })
sign({ name = 'DiagnosticSignInfo', text = '' })

vim.diagnostic.config({
    virtual_text = true,
    signs = true,
    update_in_insert = true,
    underline = true,
    severity_sort = false,
    float = {
        border = 'rounded',
        source = 'always',
        header = '',
        prefix = '',
    },
})

vim.cmd([[
set signcolumn=yes
autocmd CursorHold * lua vim.diagnostic.open_float(nil, { focusable = false })
]])

require('rust-tools').inlay_hints.enable(
)


local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())
local lsp_defaults = require("lspconfig").util.default_config

lsp_defaults.capabilities = vim.tbl_deep_extend(
    'force',
    lsp_defaults.capabilities,
    require('cmp_nvim_lsp').default_capabilities()
)
-- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
require('lspconfig')['clangd'].setup {
    capabilities = lsp_defaults.capabilities,
    filetypes = { "c", "cpp", "cu", "h", "hpp", "cuh", "cuda" },
    cmd = { "clangd", '--background-index', '--clang-tidy' }
}
--require('lspconfig')['clangd'].setup {
--capabilities = capabilities,
--}

require('lspconfig')['pyright'].setup {
    capabilities = lsp_defaults.capabilities
}
require('lspconfig')['gopls'].setup {
    capabilities = lsp_defaults.capabilities
}
--require('lspconfig')['rust_analyzer'].setup {
--    capabilities = lsp_defaults.capabilities,
--    cargo = { buildScripts = { enable = true } },
--    checkOnSave = { command = "clippy" },
--
--}
require('lspconfig').lua_ls.setup({
    settings = {
        Lua = {
            completion = {
                callSnippet = "Replace"
            }
        }
    }
})



require('nvim-autopairs').setup({
    disable_filetype = { "TelescopePrompt", "vim" },
})

require('nvim_comment').setup()


require("notify").setup({
    background_colour = "#000000",
})
vim.cmd [[highlight Headline1 guibg=#1e2718]]
vim.cmd [[highlight Headline2 guibg=#21262d]]
vim.cmd [[highlight CodeBlock guibg=#1c1c1c]]
vim.cmd [[highlight Dash guibg=#D19A66 gui=bold]]

require("conform").setup({
    formatters_by_ft = {
        lua = { "stylua" },
        -- Conform will run multiple formatters sequentially
        python = { "black" },
        cpp = { "clang-format" },
        rs = { "rustfmt" },
        -- Use a sub-list to run only the first available formatter
    },
    format_on_save = {
        -- These options will be passed to conform.format()
        timeout_ms = 500,
        lsp_fallback = true,
    },
})
require('lint').linters_by_ft = {

    python = { 'pylint', },
}
vim.api.nvim_create_autocmd({ "BufWritePost", "InsertLeave" }, {
    callback = function()
        require("lint").try_lint()
    end,
})

opts = {
    auto_set_mode_filetype_allowlist = {
        "gitcommit",
    },
    softener = { gitcommit = false },
}
require("wrapping").setup(opts)


vim.cmd([[
au FileType gitcommit setlocal tw=72
]])


local dap, dapui = require("dap"), require("dapui")
dap.listeners.after.event_initialized["dapui_config"] = function()
    dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
    dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
    dapui.close()
end
dap.adapters.lldb = {
    type = 'executable',
    command = 'lldb-vscode', -- adjust as needed, must be absolute path
    name = 'lldb'
}
dap.configurations.cpp = {
    {
        name = 'Launch',
        type = 'lldb',
        request = 'launch',
        program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
        end,
        cwd = '${workspaceFolder}',
        stopOnEntry = false,
        args = {},

        -- 💀
        -- if you change `runInTerminal` to true, you might need to change the yama/ptrace_scope setting:
        --
        --    echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
        --
        -- Otherwise you might get the following error:
        --
        --    Error on launch: Failed to attach to the target process
        --
        -- But you should be aware of the implications:
        -- https://www.kernel.org/doc/html/latest/admin-guide/LSM/Yama.html
        -- runInTerminal = false,
    },
    {
        -- If you get an "Operation not permitted" error using this, try disabling YAMA:
        --  echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
        name = "Attach to process",
        type = 'lldb', -- Adjust this to match your adapter name (`dap.adapters.<name>`)
        request = 'attach',
        pid = require('dap.utils').pick_process,
        args = {},
    },
}

dap.configurations.c = dap.configurations.cpp
dap.configurations.rust = dap.configurations.cpp

-- If you want to use this for Rust and C, add something like this:

require('dap-python').setup('/usr/bin/python')
require('dap-python').test_runner = 'pytest'
require("dapui").setup()
local dap, dapui = require("dap"), require("dapui")
dap.listeners.after.event_initialized["dapui_config"] = function()
    dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
    dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
    dapui.close()
end
require("toggleterm").setup {
    open_mapping = [[<C-y>]],
    direction = 'float',
    insert_mappings = true, -- whether or not the open mapping applies in insert mode
    terminal_mappings = true,
    start_in_insert = true,
}


require("neo-tree").setup({
    close_if_last_window = false, -- Close Neo-tree if it is the last window left in the tab
    popup_border_style = "rounded",
    enable_git_status = true,
    enable_diagnostics = true,
    open_files_do_not_replace_types = { "terminal", "trouble", "qf" }, -- when opening files, do not use windows containing these filetypes or buftypes
    sort_case_insensitive = false,                                     -- used when sorting files and directories in the tree
    sort_function = nil,                                               -- use a custom function for sorting files and directories in the tree
    -- sort_function = function (a,b)
    --       if a.type == b.type then
    --           return a.path > b.path
    --       else
    --           return a.type > b.type
    --       end
    --   end , -- this sorts files and directories descendantly
event_handlers = {
  {
    event = "neo_tree_popup_input_ready",
    ---@param args { bufnr: integer, winid: integer }
    handler = function(args)
      vim.cmd("stopinsert")
      vim.keymap.set("i", "<esc>", vim.cmd.stopinsert, { noremap = true, buffer = args.bufnr })
    end,
  }
},
    default_component_configs = {
        container = {
            enable_character_fade = true
        },
        indent = {
            indent_size = 2,
            padding = 1, -- extra padding on left hand side
            -- indent guides
            with_markers = true,
            indent_marker = "│",
            last_indent_marker = "└",
            highlight = "NeoTreeIndentMarker",
            -- expander config, needed for nesting files
            with_expanders = nil, -- if nil and file nesting is enabled, will enable expanders
            expander_collapsed = "",
            expander_expanded = "",
            expander_highlight = "NeoTreeExpander",
        },
        icon = {
            folder_closed = "",
            folder_open = "",
            folder_empty = "󰜌",
            -- The next two settings are only a fallback, if you use nvim-web-devicons and configure default icons there
            -- then these will never be used.
            default = "*",
            highlight = "NeoTreeFileIcon"
        },
        modified = {
            symbol = "[+]",
            highlight = "NeoTreeModified",
        },
        name = {
            trailing_slash = false,
            use_git_status_colors = true,
            highlight = "NeoTreeFileName",
        },
        git_status = {
            symbols = {
                -- Change type
                added     = "", -- or "✚", but this is redundant info if you use git_status_colors on the name
                modified  = "", -- or "", but this is redundant info if you use git_status_colors on the name
                deleted   = "✖", -- this can only be used in the git_status source
                renamed   = "󰁕", -- this can only be used in the git_status source
                -- Status type
                untracked = "",
                ignored   = "",
                unstaged  = "󰄱",
                staged    = "",
                conflict  = "",
            }
        },
        -- If you don't want to use these columns, you can set `enabled = false` for each of them individually
        file_size = {
            enabled = true,
            required_width = 64, -- min width of window required to show this column
        },
        type = {
            enabled = true,
            required_width = 122, -- min width of window required to show this column
        },
        last_modified = {
            enabled = true,
            required_width = 88, -- min width of window required to show this column
        },
        created = {
            enabled = true,
            required_width = 110, -- min width of window required to show this column
        },
        symlink_target = {
            enabled = false,
        },
    },
    -- A list of functions, each representing a global custom command
    -- that will be available in all sources (if not overridden in `opts[source_name].commands`)
    -- see `:h neo-tree-custom-commands-global`
    commands = {},
    window = {
        position = "left",
        width = 40,
        mapping_options = {
            noremap = true,
            nowait = true,
        },
        mappings = {
            ["<space>"] = {
                "toggle_node",
                nowait = false, -- disable `nowait` if you have existing combos starting with this char that you want to use
            },
            ["<2-LeftMouse>"] = "open",
            ["<cr>"] = "open",
            ["<esc>"] = "cancel", -- close preview or floating neo-tree window
            ["P"] = { "toggle_preview", config = { use_float = true } },
            ["l"] = "focus_preview",
            ["S"] = "open_split",
            ["s"] = "open_vsplit",
            -- ["S"] = "split_with_window_picker",
            -- ["s"] = "vsplit_with_window_picker",
            ["t"] = "open_tabnew",
            -- ["<cr>"] = "open_drop",
            -- ["t"] = "open_tab_drop",
            ["w"] = "open_with_window_picker",
            --["P"] = "toggle_preview", -- enter preview mode, which shows the current node without focusing
            ["C"] = "close_node",
            -- ['C'] = 'close_all_subnodes',
            ["z"] = "close_all_nodes",
            --["Z"] = "expand_all_nodes",
            ["a"] = {
                "add",
                -- this command supports BASH style brace expansion ("x{a,b,c}" -> xa,xb,xc). see `:h neo-tree-file-actions` for details
                -- some commands may take optional config options, see `:h neo-tree-mappings` for details
                config = {
                    show_path = "none" -- "none", "relative", "absolute"
                }
            },
            ["A"] = "add_directory", -- also accepts the optional config.show_path option like "add". this also supports BASH style brace expansion.
            ["d"] = "delete",
            ["r"] = "rename",
            ["y"] = "copy_to_clipboard",
            ["x"] = "cut_to_clipboard",
            ["p"] = "paste_from_clipboard",
            ["c"] = "copy", -- takes text input for destination, also accepts the optional config.show_path option like "add":
            -- ["c"] = {
            --  "copy",
            --  config = {
            --    show_path = "none" -- "none", "relative", "absolute"
            --  }
            --}
            ["m"] = "move", -- takes text input for destination, also accepts the optional config.show_path option like "add".
            ["q"] = "close_window",
            ["R"] = "refresh",
            ["?"] = "show_help",
            ["<"] = "prev_source",
            [">"] = "next_source",
            ["i"] = "show_file_details",
        }
    },
    nesting_rules = {},
    filesystem = {
        filtered_items = {
            visible = false, -- when true, they will just be displayed differently than normal items
            hide_dotfiles = true,
            hide_gitignored = true,
            hide_hidden = true, -- only works on Windows for hidden files/directories
            hide_by_name = {
                --"node_modules"
            },
            hide_by_pattern = { -- uses glob style patterns
                --"*.meta",
                --"*/src/*/tsconfig.json",
            },
            always_show = { -- remains visible even if other settings would normally hide it
                --".gitignored",
            },
            never_show = { -- remains hidden even if visible is toggled to true, this overrides always_show
                --".DS_Store",
                --"thumbs.db"
            },
            never_show_by_pattern = { -- uses glob style patterns
                --".null-ls_*",
            },
        },
        follow_current_file = {
            enabled = false,                    -- This will find and focus the file in the active buffer every time
            --               -- the current file is changed while the tree is open.
            leave_dirs_open = false,            -- `false` closes auto expanded dirs, such as with `:Neotree reveal`
        },
        group_empty_dirs = false,               -- when true, empty folders will be grouped together
        hijack_netrw_behavior = "open_default", -- netrw disabled, opening a directory opens neo-tree
        -- in whatever position is specified in window.position
        -- "open_current",  -- netrw disabled, opening a directory opens within the
        -- window like netrw would, regardless of window.position
        -- "disabled",    -- netrw left alone, neo-tree does not handle opening dirs
        use_libuv_file_watcher = false, -- This will use the OS level file watchers to detect changes
        -- instead of relying on nvim autocmd events.
        window = {
            mappings = {
                ["<bs>"] = "navigate_up",
                ["."] = "set_root",
                ["H"] = "toggle_hidden",
                ["/"] = "fuzzy_finder",
                ["D"] = "fuzzy_finder_directory",
                ["#"] = "fuzzy_sorter", -- fuzzy sorting using the fzy algorithm
                -- ["D"] = "fuzzy_sorter_directory",
                ["f"] = "filter_on_submit",
                ["<c-x>"] = "clear_filter",
                ["[g"] = "prev_git_modified",
                ["]g"] = "next_git_modified",
                ["o"] = { "show_help", nowait = false, config = { title = "Order by", prefix_key = "o" } },
                ["oc"] = { "order_by_created", nowait = false },
                ["od"] = { "order_by_diagnostics", nowait = false },
                ["og"] = { "order_by_git_status", nowait = false },
                ["om"] = { "order_by_modified", nowait = false },
                ["on"] = { "order_by_name", nowait = false },
                ["os"] = { "order_by_size", nowait = false },
                ["ot"] = { "order_by_type", nowait = false },
            },
            fuzzy_finder_mappings = { -- define keymaps for filter popup window in fuzzy_finder_mode
                ["<down>"] = "move_cursor_down",
                ["<C-n>"] = "move_cursor_down",
                ["<up>"] = "move_cursor_up",
                ["<C-p>"] = "move_cursor_up",
            },
        },

        commands = {} -- Add a custom command or override a global one using the same function name
    },
    buffers = {
        follow_current_file = {
            enabled = true,          -- This will find and focus the file in the active buffer every time
            --              -- the current file is changed while the tree is open.
            leave_dirs_open = false, -- `false` closes auto expanded dirs, such as with `:Neotree reveal`
        },
        group_empty_dirs = true,     -- when true, empty folders will be grouped together
        show_unloaded = true,
        window = {
            mappings = {
                ["bd"] = "buffer_delete",
                ["<bs>"] = "navigate_up",
                ["."] = "set_root",
                ["o"] = { "show_help", nowait = false, config = { title = "Order by", prefix_key = "o" } },
                ["oc"] = { "order_by_created", nowait = false },
                ["od"] = { "order_by_diagnostics", nowait = false },
                ["om"] = { "order_by_modified", nowait = false },
                ["on"] = { "order_by_name", nowait = false },
                ["os"] = { "order_by_size", nowait = false },
                ["ot"] = { "order_by_type", nowait = false },
            }
        },
    },
    git_status = {
        window = {
            position = "float",
            mappings = {
                ["A"]  = "git_add_all",
                ["gu"] = "git_unstage_file",
                ["ga"] = "git_add_file",
                ["gr"] = "git_revert_file",
                ["gc"] = "git_commit",
                ["gp"] = "git_push",
                ["gg"] = "git_commit_and_push",
                ["o"]  = { "show_help", nowait = false, config = { title = "Order by", prefix_key = "o" } },
                ["oc"] = { "order_by_created", nowait = false },
                ["od"] = { "order_by_diagnostics", nowait = false },
                ["om"] = { "order_by_modified", nowait = false },
                ["on"] = { "order_by_name", nowait = false },
                ["os"] = { "order_by_size", nowait = false },
                ["ot"] = { "order_by_type", nowait = false },
            }
        }
    }
})
vim.cmd [[set indentexpr=nvim_treesitter#indent()]]
utils.opt('b', 'smartindent', true)
--require('neorg').setup {
--    load = {
--        ["core.defaults"] = {},
--        ["core.concealer"] = {},
--        -- not available with this version of neovim["core.tempus"] = {},
--        ["core.keybinds"] = { config = { default_keybinds = true, }, },
--        ["core.esupports.metagen"] = { config = { type = "auto", update_date = true } },
--        ["core.integrations.telescope"] = {},
--        ["core.completion"] = { config = { engine = "nvim-cmp", name = "[Norg]" } },
--        ["core.integrations.nvim-cmp"] = {},
--        ["core.export"] = {},
--        ["core.summary"] = {},
--        ["core.qol.todo_items"] = {},
--        ["core.looking-glass"] = {},
--        ["core.presenter"] = { config = { zen_mode = "truezen" } },
--        }
--    }
--local neorg_callbacks = require("neorg.core.callbacks")
--
--neorg_callbacks.on_event("core.keybinds.events.enable_keybinds", function(_, keybinds)
--    -- Map all the below keybinds only when the "norg" mode is active
--    keybinds.map_event_to_mode("norg", {
--        n = { -- Bind keys in normal mode
--            { "<C-s>", "core.integrations.telescope.find_linkable" },
--        },
--
--        i = { -- Bind in insert mode
--            { "<C-l>", "core.integrations.telescope.insert_link" },
--        },
--    }, {
--        silent = true,
--        noremap = true,
--    })
--end)
vim.api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
    pattern = { "*.norg" },
    command = "set conceallevel=3"
})
dap.configurations.rust = {
    {
        -- ... the previous config goes here ...,
        name = 'Launch',
        type = 'lldb',
        request = 'launch',
        program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
        end,
        cwd = '${workspaceFolder}',
        stopOnEntry = false,
        args = {},
        initCommands = function()
            -- Find out where to look for the pretty printer Python module
            local rustc_sysroot = vim.fn.trim(vim.fn.system('rustc --print sysroot'))

            local script_import = 'command script import "' .. rustc_sysroot .. '/lib/rustlib/etc/lldb_lookup.py"'
            local commands_file = rustc_sysroot .. '/lib/rustlib/etc/lldb_commands'

            local commands = {}
            local file = io.open(commands_file, 'r')
            if file then
                for line in file:lines() do
                    table.insert(commands, line)
                end
                file:close()
            end
            table.insert(commands, 1, script_import)

            return commands
        end,
        -- ...,
    }
}
require("nvim-treesitter.install").prefer_git = true

local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
    return
end

configs.setup {
    sync_install = true,
    ignore_install = { "" },
    highlight = {
        enable = true,
        disable = { "" },
        additional_vim_regex_highlighting = { "cpp" },
    },
    rainbow = {
        enable = true,
        extended_mode = true,
        max_file_lines = nil,
    },
    textobjects = {
        swap = {
            enable = true,
            swap_next = {
                ["<leader>a"] = "@parameter.inner",
            },
            swap_previous = {
                ["<leader>A"] = "@parameter.inner",
            },
        },
        select = {
            enable = true,

            -- Automatically jump forward to textobj, similar to targets.vim
            lookahead = true,

            keymaps = {
                -- You can use the capture groups defined in textobjects.scm
                ["af"] = { query = "@function.outer", desc = "select outer function" },
                ["if"] = { query = "@function.inner", desc = "select inner function" },
                ["ac"] = { query = "@class.outer", desc = "select outer class" },
                -- You can optionally set descriptions to the mappings (used in the desc parameter of
                -- nvim_buf_set_keymap) which plugins like which-key display
                ["ic"] = { query = "@class.inner", desc = "Select inner part of a class region" },
                -- You can also use captures from other query groups like `locals.scm`
                ["as"] = { query = "@scope", query_group = "locals", desc = "Select language scope" },
            },
            -- You can choose the select mode (default is charwise 'v')
            --
            -- Can also be a function which gets passed a table with the keys
            -- * query_string: eg '@function.inner'
            -- * method: eg 'v' or 'o'
            -- and should return the mode ('v', 'V', or '<c-v>') or a table
            -- mapping query_strings to modes.
            selection_modes = {
                ['@parameter.outer'] = 'v', -- charwise
                ['@function.outer'] = 'v',  -- linewise
                ['@class.outer'] = 'v',     -- blockwise
            },
            -- If you set this to `true` (default is `false`) then any textobject is
            -- extended to include preceding or succeeding whitespace. Succeeding
            -- whitespace has priority in order to act similarly to eg the built-in
            -- `ap`.
            --
            -- Can also be a function which gets passed a table with the keys
            -- * query_string: eg '@function.inner'
            -- * selection_mode: eg 'v'
            -- and should return true of false
            include_surrounding_whitespace = true,
        },
        move = {
            enable = true,
            set_jumps = true, -- whether to set jumps in the jumplist
            goto_next_start = {
                ["]m"] = "@function.outer",
                ["]]"] = { query = "@class.outer", desc = "Next class start" },
                --
                -- You can use regex matching (i.e. lua pattern) and/or pass a list in a "query" key to group multiple queires.
                ["]o"] = "@loop.*",
                -- ["]o"] = { query = { "@loop.inner", "@loop.outer" } }
                --
                -- You can pass a query group to use query from `queries/<lang>/<query_group>.scm file in your runtime path.
                -- Below example nvim-treesitter's `locals.scm` and `folds.scm`. They also provide highlights.scm and indent.scm.
                ["]s"] = { query = "@scope", query_group = "locals", desc = "Next scope" },
                ["]z"] = { query = "@fold", query_group = "folds", desc = "Next fold" },
            },
            goto_next_end = {
                ["]M"] = "@function.outer",
                ["]["] = "@class.outer",
            },
            goto_previous_start = {
                ["[m"] = "@function.outer",
                ["[["] = "@class.outer",
            },
            goto_previous_end = {
                ["[M"] = "@function.outer",
                ["[]"] = "@class.outer",
            },
            -- Below will go to either the start or the end, whichever is closer.
            -- Use if you want more granular movements
            -- Make it even more gradual by adding multiple queries and regex.
            goto_next = {
                ["]d"] = "@conditional.outer",
            },
            goto_previous = {
                ["[d"] = "@conditional.outer",
            }
        },
    },
}

local api = vim.api
local M = {}
-- function to create a list of commands and convert them to autocommands
-------- This function is taken from https://github.com/norcalli/nvim_utils
function M.nvim_create_augroups(definitions)
    for group_name, definition in pairs(definitions) do
        api.nvim_command('augroup ' .. group_name)
        api.nvim_command('autocmd!')
        for _, def in ipairs(definition) do
            local command = table.concat(vim.iter(…):flatten():totable(){ 'autocmd', def }, ' ')
            api.nvim_command(command)
        end
        api.nvim_command('augroup END')
    end
end

local autoCommands = {
    open_folds = {
        { "BufReadPost,FileReadPost", "*", "normal zR" }
    }
}
    require('lspconfig').nil_ls.setup {
      autostart = true,
      capabilities = capabilities,
      cmd = { "nil" },
      settings = {
        ['nil'] = {
          testSetting = 42,
          formatting = {
            command = { "nixpkgs-fmt" },
          },
        },
    },
  }


--require('orgmode').setup_ts_grammar()

--local llm = require('llm')
--
--llm.setup({
--  api_token = "ollama", -- cf Install paragraph
--  backend = "ollama", -- backend ID, "huggingface" | "ollama" | "openai" | "tgi"
--  --url = nil, -- the http url of the backend
--  url = "http://devnode11.rnd:11434/api",
--  --
--  tokens_to_clear = { "<|endoftext|>" }, -- tokens to remove from the model's output
--  -- parameters that are added to the request body, values are arbitrary, you can set any field:value pair here it will be passed as is to the backend
--  model = "magicoder:latest",
--  request_body = {
--      temperature = 0.2,
--      top_p = 0.95,
--  },
--  -- siet this if the model supports fill in the middle
--  --fim = {
--    --enabled = true,
--    --prefix = "<fim_prefix>",
--    --middle = "<fim_middle>",
--    --suffix = "<fim_suffix>",
--  --},
--  debounce_ms = 500,
--  accept_keymap = "<Tab>",
--  dismiss_keymap = "<S-Tab>",
--  tls_skip_verify_insecure = false,
--  -- llm-ls configuration, cf llm-ls section
--  tokenizer = nil, -- cf Tokenizer paragraph
--  context_window = 8192, -- max number of tokens for the context window
--  enable_suggestions_on_startup = true,
--
--enable_suggestions_on_files = {"*.cpp", "*.hpp", "*.cu", "*.cuh", "*.py", "*.rs", "*.lua"}, -- pattern matching syntax to enable suggestions on specific files, either a string or a list of strings
--  
--enable_suggestions_on_files = "*",
--
--})
